Minetest mod: easteregg
=======================
See license.txt for license information.

This mod adds 14 colored eggs. The recipe uses eggs from mobs_animals and dyes from dyes.

Authors of source code
----------------------
Hume2 (MIT)

Authors of media (textures and model)
-------------------------------------
Hume2 (MIT)

