
local function register_egg(id, name, colour, craftitem)
	minetest.register_node("easteregg:" .. id .. "_egg", {
		description = name .. " Egg",
		tiles = {"easteregg_back.png^(easteregg_front.png^[colorize:#" .. colour .. ")"},
		groups = {dig_immediate = 2},
		paramtype = "light",
		drawtype = "mesh",
		mesh = "easteregg_egg.obj",
		is_ground_content = false,
		sounds = default.node_sound_glass_defaults(),
		collision_box = {
			type = "fixed",
			fixed = {-0.3, -0.4, -0.3, 0.3, 0.3, 0.3},
		},
		selection_box = {
			type = "fixed",
			fixed = {-0.3, -0.4, -0.3, 0.3, 0.3, 0.3},
		},
	})

	minetest.register_craft({
		recipe = {"mobs:egg", craftitem},
		type = "shapeless",
		output = "easteregg:" .. id .. "_egg",
	})
end

register_egg("grey", "Grey", "808080", "dye:grey")
register_egg("dark_grey", "Dark Grey", "404040", "dye:dark_grey")
register_egg("black", "Black", "000000", "dye:black")
register_egg("violet", "Violet", "8000FF", "dye:violet")
register_egg("blue", "Blue", "0000FF", "dye:blue")
register_egg("cyan", "Cyan", "008080", "dye:cyan")
register_egg("dark_green", "Dark Green", "008000", "dye:dark_green")
register_egg("green", "Green", "00FF00", "dye:green")
register_egg("yellow", "Yellow", "FFFF00", "dye:yellow")
register_egg("brown", "Brown", "804000", "dye:brown")
register_egg("orange", "Orange", "FF8000", "dye:orange")
register_egg("red", "Red", "FF0000", "dye:red")
register_egg("magenta", "Magenta", "FF00FF", "dye:magenta")
register_egg("pink", "Pink", "FF8080", "dye:pink")

